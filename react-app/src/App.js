// Importables
import {useState} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar.js';
import {Container} from 'react-bootstrap';
import './App.css';
import { UserProvider } from './UserContext.js';
import Home from './pages/Home.js';
import Doors from './pages/Doors.js';
import CustomerOrders from './pages/CustomerOrder.js';
import DoorCardView from './components/DoorCardView.js';
import Admin from './pages/Admin.js'
import Login from './pages/Login.js';
import Register from './pages/Register.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import CreateCard from './components/CreateCard.js'
import Create from './pages/Create.js';
import Update from './pages/Update.js'; 



// Component function
function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null 
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  
  return (
    
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
      
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/doors" element={<Doors/>}/>
            <Route path="/doors/:doorId/view" element={<DoorCardView/>}/>
            <Route path="/orders/:userId" element={<CustomerOrders/>}/>
            <Route path="/orders/:userId/admin" element={<CustomerOrders/>}/>
            <Route path="/doors/:userId/admin" element={<Admin/>}/>  
            <Route path="/doors/:userId/" element={<Create/>}/>
            <Route path="/doors/:doorId/update" element={<Update/>}/>
            <Route path="/login" element={<Login/>}/>           
            <Route path="/register" element={<Register/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="*" element={<NotFound/>}/>

          </Routes>
        </Container>
      </Router> 
    </UserProvider>
  );
}

// Exporting of the component function
export default App;


