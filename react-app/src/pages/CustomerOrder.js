import { useState, useEffect } from 'react';
import CustomerOrderCard from '../components/CustomerOrderCard';
import LoadingSpinner from '../components/LoadingSpinner';
import { Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

export default function CustomerOrders() {
  const [orders, setOrders] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const { userId } = useParams();

  useEffect(() => {
  setIsLoading(true);

  const token = localStorage.getItem('token');
  
  if (!token) {
    // Handle the case when the user is not authenticated
    setIsLoading(false);
    // Redirect the user to the login page or display an error message
    return;
  }

  fetch(`${process.env.REACT_APP_API_URL}/orders/${userId}`, {
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
    .then(response => response.json())
    .then(result => {
      setIsLoading(false);

      if (Array.isArray(result)) {
        setOrders(result.map(order => (
          <CustomerOrderCard key={order._id} order={order} />
        )));
      }
    })
    .catch(error => {
      console.log(error);
    });
}, [userId]);

  return (
    <Row>
      <Col>
        {isLoading ? (
          <LoadingSpinner />
        ) : (
          <Row>
            {orders.length > 0 ? (
              orders.map((order, index) => (
                <Col key={index} lg={12}>
                  {order}
                </Col>
              ))
            ) : (
              <h1>No orders found.</h1>
            )}
          </Row>
        )}
      </Col>
    </Row>
  );
}
