import { useState, useEffect } from 'react';
import AdminDashboard from '../components/AdminDashboard';
import LoadingSpinner from '../components/LoadingSpinner';
import { Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom';

export default function Admin() {
  const [doors, setDoors] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const { userId } = useParams();

  useEffect(() => {
    setIsLoading(true);

    const token = localStorage.getItem('token');
  
    if (!token) {
      // Handle the case when the user is not authenticated
      setIsLoading(false);
      // Redirect the user to the login page or display an error message
      return;
    }

    // to get all Doors
    fetch(`${process.env.REACT_APP_API_URL}/doors/${userId}/admin`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);

        if (Array.isArray(result)) {
          setDoors(result);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }, [userId]);

  

  return (
    <Row>
      <Col>
        {isLoading ? (
          <LoadingSpinner />
        ) : (
          <Row>
          <h1> Admin Dashboard </h1>
          <Link className="btn btn-primary" to={`/doors/${userId}/`}>Create</Link>
            {doors.length > 0 ? (
              doors.map(door => (
                <Col key={door._id} lg={12}>
                  <AdminDashboard door={door} userId={userId} />
                </Col>
              ))
            ) : (
              <h1>No orders found.</h1>
            )}
          </Row>
        )}
      </Col>
    </Row>
  );
}
