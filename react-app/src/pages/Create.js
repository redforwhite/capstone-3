import { Container, Row, Col } from 'react-bootstrap';
import CreateCard from '../components/CreateCard';
import { useParams } from 'react-router-dom';

export default function Create() {
  const { userId } = useParams();
  const handleCreateDoor = (newDoor) => {
    const token = localStorage.getItem('token');
    
    fetch(`${process.env.REACT_APP_API_URL}/doors/${userId}/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(newDoor),
    })
      .then((response) => response.json())
      .then((result) => {
        // Handle the API response
        console.log('New door created:', result);
      })
      .catch((error) => {
        // Handle any errors
        console.log('API request failed:', error);
      });
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <h1>Create Door</h1>
          <CreateCard onCreate={handleCreateDoor} />
        </Col>
      </Row>
    </Container>
  );
}
