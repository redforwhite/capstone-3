import { Fragment } from 'react';
import Banner from '../components/Banner';
import DoorCarousel from '../components/Carousel';

export default function Home() {
  return (
    <Fragment>
      <div style={{ display: 'flex', alignItems: 'center', height: '90vh' }}>
        <div style={{ width: '50%', textAlign: 'left' }}>
          <Banner />
        </div>
      	<div style={{ width: '50%', textAlign: 'right' }}>
      		<DoorCarousel />
      	</div>
      </div>
    </Fragment>
  );
}
