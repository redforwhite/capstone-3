import { useState, useEffect } from 'react';
import DoorCard from '../components/DoorCard';
import LoadingSpinner from '../components/LoadingSpinner';
import { Row, Col } from 'react-bootstrap';

export default function Doors() {
  const [doors, setDoors] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);

    fetch(`${process.env.REACT_APP_API_URL}/doors/active`)
      .then(response => response.json())
      .then(result => {
        console.log(result);
        setIsLoading(false);

        setDoors(result.map((door, index) => {
          const isLeftAligned = index % 2 === 0;

          return (
            <DoorCard key={door._id} door={door} isLeftAligned={isLeftAligned} />
          );
        }));
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  return (
    <Row>
      <Col>
        {isLoading ? (
          <LoadingSpinner />
        ) : (
          <Row>
            {doors.map((door, index) => (
              <Col key={index} lg={6}>
                {door}
              </Col>
            ))}
          </Row>
        )}
      </Col>
    </Row>
  );
}
