import { Container, Row, Col } from 'react-bootstrap';
import UpdateCard from '../components/UpdateCard';
import { useParams } from 'react-router-dom';

export default function Update() {
  const { userId, doorId } = useParams();
  const handleUpdateDoor = (updatedDoor) => {
    const token = localStorage.getItem('token');

  // Fetch the existing door details
    fetch(`${process.env.REACT_APP_API_URL}/doors/${doorId}/view`)
    .then((response) => response.json())
    .then((existingDoor) => {
      // Update the door object with the updated data
      const updatedDoorData = {
        name: updatedDoor.name,
        description: updatedDoor.description,
        price: updatedDoor.price,
        quantity: updatedDoor.quantity
      };

      // Perform the patch request to update the door
      fetch(`${process.env.REACT_APP_API_URL}/doors/${userId}/${doorId}/update`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(updatedDoorData),
      })
      .then((response) => response.json())
      .then((result) => {
          // Handle the API response
        console.log('Door updated:', result);
      })
      .catch((error) => {
          // Handle any errors
        console.log('API request failed:', error);
      });
    })
    .catch((error) => {
      // Handle any errors
      console.log('Failed to fetch door:', error);
    });
  };

  return (
    <Container className="mt-5">
    <Row>
    <Col lg={{ span: 6, offset: 3 }}>
    <h1>Update Door</h1>
    <UpdateCard onUpdate={handleUpdateDoor} />
    </Col>
    </Row>
    </Container>
    );
}
