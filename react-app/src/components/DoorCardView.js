import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function DoorCardView() {
  const navigate = useNavigate();

  const { user } = useContext(UserContext);

  const { doorId, userId } = useParams();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [quantity, setQuantity] = useState(0);
  const [price, setPrice] = useState(0);
  const [isLoading, setIsLoading] = useState(true);

  const token = localStorage.getItem('token');

  useEffect(() => {
    if (!token) {
      setIsLoading(false);
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/doors/${doorId}/view`)
      .then((response) => response.json())
      .then((result) => {
        setName(result.name);
        setDescription(result.description);
        setPrice(result.price);
        setQuantity(result.quantity);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log('API request failed:', error);
        setIsLoading(false);
      });
  }, [doorId, token]);

  const order = () => {
    console.log('Order button clicked');

    fetch(`${process.env.REACT_APP_API_URL}/orders/${userId}/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        userId: userId,
      }),
    })
      .then((response) => {
        console.log('API request sent');
        return response.json();
      })
      .then((result) => {
        console.log('API response received:', result);
        // Rest of the code
      })
      .catch((error) => {
        console.log('API request failed:', error);
      });
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card className="my-3">
            <Card.Body>
              <Card.Title>{name}</Card.Title>

              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>

              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>Php {price}</Card.Text>

              <Card.Subtitle>Quantity:</Card.Subtitle>
              <Card.Text>{quantity}</Card.Text>

              {user.id ? (
                <Button variant="primary" onClick={order}>
                  Order
                </Button>
              ) : (
                <Link className="btn btn-warning" to="/login">
                  Log In to Order
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
