import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2'

export default function AdminDashboard({ door, userId }) {
  if (!door) {
    // Return null or handle the case when door is null
    return null;
  }

  const { _id, name, description, price, quantity, isActive, createdOn } = door;

  const archiveDoor = (doorId) => {
    const token = localStorage.getItem('token');

    fetch(`${process.env.REACT_APP_API_URL}/doors/${userId}/${_id}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        console.log('Door archived:', result);

          Swal.fire({
          title: "Door archived",
          icon: "success",
          text: "Archived"
        })
      })
      .catch((error) => {
        console.log('Archive door error:', error);
        // Handle the error if the door archiving fails
      });
  };

 const activateDoor = (doorId) => {
    const token = localStorage.getItem('token');

    fetch(`${process.env.REACT_APP_API_URL}/doors/${userId}/${_id}/activate`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    body: JSON.stringify({
      isActive: true,
    }),

    })
      .then((response) => response.json())
      .then((result) => {
        console.log('Door activated:', result);

          Swal.fire({
          title: "Door activated",
          icon: "success",
          text: "Activated"
        })
      })
      .catch((error) => {
        console.log('Activate door error:', error);
        // Handle the error if the door activation fails
      });
  };


  return (
    <Card className="my-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>

        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>

        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>Php {price}</Card.Text>

        <Card.Subtitle>Quantity:</Card.Subtitle>
        <Card.Text>{quantity}</Card.Text>

        <Card.Subtitle>Active:</Card.Subtitle>
        <Card.Text>{isActive ? isActive.toString() : ''}</Card.Text>

        <Card.Subtitle>Created On:</Card.Subtitle>
        <Card.Text>{createdOn}</Card.Text>

        <Link className="btn btn-primary" to={`/doors/${userId}/update`}>
          Update
        </Link>

        <Link className="btn btn-primary" onClick={() => activateDoor(_id)}>
          Activate
        </Link>

        <Link className="btn btn-primary" onClick={() => archiveDoor(_id)}>
          Archive
        </Link>
      </Card.Body>
    </Card>
  );
}

AdminDashboard.propTypes = {
  door: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    isActive: PropTypes.bool.isRequired,
    createdOn: PropTypes.instanceOf(Date),
  }),
};


