import { Fragment, useContext } from 'react';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {
  // Gets the user email after the user has logged in
  const { user } = useContext(UserContext);

  return (
    <Navbar expand="lg" style={{ backgroundColor: '#546460' }}>
      <Container fluid>
        <Navbar.Brand as={Link} to="/" className="portal-brand">
          Portal
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="custom-nav ms-auto justify-content-end">
            <Nav.Link as={NavLink} to="/" className="nav-link" style={{ color: '#D9BC22' }}>
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/doors" className="nav-link" style={{ color: '#EAC919' }}>
              Doors
            </Nav.Link>

            {user.id !== null ? (
              // If the user has logged in
              <NavDropdown title={<span style={{ color: '#FDDF3E' }}>Profile</span>} id="basic-nav-dropdown">
                <Fragment>
                  <Nav.Link as={NavLink} to="/logout" className="nav-link text-center" style={{ color: '#EF7029' }}>
                    Logout
                  </Nav.Link>
                  {user.isAdmin ? (
                    <Fragment>
                      <Nav.Link
                        as={NavLink}
                        to={`/orders/${user.id}/admin`}
                        className="nav-link text-center"
                        style={{ color: '#CF103E' }}
                      >
                        Orders
                      </Nav.Link>
                      <NavDropdown title={<span style={{ color: '#FDDF3E' }}>Admin</span>} className="nav-link text-center" id="basic-nav-dropdown">
                        <Fragment>
                          <Nav.Link
                            as={NavLink}
                            to={`/doors/${user.id}/admin`}
                            className="nav-link text-center"
                            style={{ color: '#CF103E' }}
                          >
                            Dashboard
                          </Nav.Link>
                          
                        </Fragment>
                      </NavDropdown>
                    </Fragment>
                  ) : (
                    <Nav.Link
                      as={NavLink}
                      to={`/orders/${user.id}`}
                      className="nav-link text-center"
                      style={{ color: '#EF7029' }}
                    >
                      Orders
                    </Nav.Link>
                  )}
                </Fragment>
              </NavDropdown>
            ) : (
              // If the user has not logged in
              <NavDropdown title={<span style={{ color: '#FDDF3E' }}>Entryway</span>} id="basic-nav-dropdown">
                <Fragment>
                  <Nav.Link as={NavLink} to="/login" className="nav-link text-center">
                    Login
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/register" className="nav-link text-center">
                    Register
                  </Nav.Link>
                </Fragment>
              </NavDropdown>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
