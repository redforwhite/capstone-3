import {Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function DoorCard({door}){
 

	const {_id, name, description, price} = door


	return(
		<Card className="my-3"> 
			<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>

				<Link className="btn btn-primary" to={`/doors/${_id}/view`}>View Portal</Link>
			</Card.Body>
		</Card>
	)
}

DoorCard.propTypes = {
	door: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}


