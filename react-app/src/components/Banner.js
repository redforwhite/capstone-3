import { useState } from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {
  const [buttonStyle, setButtonStyle] = useState({
    backgroundColor: '#546460',
    borderColor: '#aad0c7',
    color: '#fddf3e',
    transition: 'background-color 0.3s, color 0.3s',
  });

  const buttonHoverStyle = {
    backgroundColor: '#aad0c7',
    color: '#546460',
  };

  const buttonActiveStyle = {
    backgroundColor: '#fddf3e',
    color: '#546460',
  };

  return (
    <Row>
      <Col className="p-5">
        <h1>PORTAL</h1>
        <p>Unlock the Gateway to Limitless Door Possibilities and Discover the Entrance to Extraordinary Spaces</p>
        <Link to="/doors">
          <Button
            style={buttonStyle}
            onMouseEnter={() => {
              setButtonStyle(buttonHoverStyle);
            }}
            onMouseLeave={() => {
              setButtonStyle({
                ...buttonStyle,
                ...{
                  backgroundColor: '#546460',
                  color: '#fddf3e',
                },
              });
            }}
            onMouseDown={() => {
              setButtonStyle(buttonActiveStyle);
            }}
            onMouseUp={() => {
              setButtonStyle(buttonHoverStyle);
            }}
          >
            Discover Passages!
          </Button>
        </Link>
      </Col>
    </Row>
  );
}
