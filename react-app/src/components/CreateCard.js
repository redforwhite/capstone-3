import { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CreateCard({ onCreate }) {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(0);

  const handleSubmit = (event) => {
    event.preventDefault();

    // Create a new door object with the form data
    const newDoor = {
      name: name,
      description: description,
      price: price,
      quantity: quantity
    };

    // Call the onCreate callback with the new door data
    onCreate(newDoor);

    // Reset the form fields
    setName('');
    setDescription('');
    setPrice(0);
    setQuantity(0);
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group controlId="name">
        <Form.Label>Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter door name"
          value={name}
          onChange={(event) => setName(event.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control
          as="textarea"
          placeholder="Enter door description"
          value={description}
          onChange={(event) => setDescription(event.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="price">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter door price"
          value={price}
          onChange={(event) => setPrice(event.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="quatity">
        <Form.Label>Quantity</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter door quantity"
          value={quantity}
          onChange={(event) => setQuantity(event.target.value)}
          required
        />
      </Form.Group>

      <Button variant="primary" type="submit">
        Create Door
      </Button>
    </Form>
  );
}

CreateCard.propTypes = {
  onCreate: PropTypes.func.isRequired
};
