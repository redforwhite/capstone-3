import Carousel from 'react-bootstrap/Carousel';
import door1 from '../doorimages/door1.png';
import door2 from '../doorimages/door2.png';
import door3 from '../doorimages/door3.png';
import '../App.css'; // Import the custom CSS file

function DoorCarousel() {
  return (
    <Carousel className="carousel-container">
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100 carousel-image"
          src={door1}
          alt="Infinity Gateway"
        />
        <Carousel.Caption>
          <h3 className="carousel-title">Infinity Gateway</h3>
          <p className="carousel-description">Where boundaries dissolve, and possibilities become infinite</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={500}>
        <img
          className="d-block w-100 carousel-image"
          src={door2}
          alt="Nexus Passage"
        />
        <Carousel.Caption>
          <h3 className="carousel-title">Nexus Passage</h3>
          <p className="carousel-description">A bridge connecting realms, bringing together the threads of existence</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100 carousel-image"
          src={door3}
          alt="Cosmic Threshold"
        />
        <Carousel.Caption>
          <h3 className="carousel-title">Cosmic Threshold</h3>
          <p className="carousel-description">
            Step beyond the stars and traverse the celestial veil
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default DoorCarousel;
