import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function CustomerOrderCard({ order }) {
  const { _id, doors, totalAmount, purchasedOn } = order;

  return (
    <Card>
      <Card.Body>
        <Card.Title>Order ID: {_id}</Card.Title>

        <Card.Subtitle>Total Amount: Php {totalAmount}</Card.Subtitle>
        <Card.Subtitle>Date Purchased: {new Date(purchasedOn).toLocaleDateString()}</Card.Subtitle>

        {doors.map((door) => (
          <div key={door.doorId}>
            <Card.Subtitle>Door ID: {door.doorId}</Card.Subtitle>
            <Card.Subtitle>Door Name: {door.doorName}</Card.Subtitle>
            <Card.Subtitle>Quantity: {door.quantity}</Card.Subtitle>
            <Card.Subtitle>Price: Php {door.price}</Card.Subtitle>
          </div>
        ))}
      </Card.Body>
    </Card>
  );
}

CustomerOrderCard.propTypes = {
  order: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    doors: PropTypes.arrayOf(
      PropTypes.shape({
        doorId: PropTypes.string.isRequired,
        doorName: PropTypes.string.isRequired,
        quantity: PropTypes.number.isRequired,
        price: PropTypes.number.isRequired,
      })
    ).isRequired,
    totalAmount: PropTypes.number.isRequired,
    purchasedOn: PropTypes.string.isRequired,
  }).isRequired,
};

